import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class MusicControlsRow extends StatefulWidget {
  AudioPlayer audioPlayer;
  final String musicFileLink;
  bool isMusicPlaying;

  MusicControlsRow({
    required this.musicFileLink,
    required this.isMusicPlaying,
    required this.audioPlayer,
  });

  @override
  _MusicControlsRowState createState() => _MusicControlsRowState();
}

class _MusicControlsRowState extends State<MusicControlsRow> {
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // widget.audioPlayer.stop();
    setState(() {
      widget.isMusicPlaying = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        !widget.isMusicPlaying
            ? IconButton(
                icon: Icon(Icons.play_arrow),
                onPressed: () {
                  _playMusic(widget.musicFileLink);
                },
              )
            : IconButton(
                icon: Icon(Icons.pause),
                onPressed: () {
                  _pauseMusic();
                },
              ),
        IconButton(
          icon: Icon(Icons.cloud_download),
          onPressed: () {
            // Ajoutez ici la logique pour télécharger la musique depuis Firebase Storage
          },
        ),
      ],
    );
  }

  Future<void> _playMusic(String musicLink) async {
    Source link = UrlSource(musicLink);
    await widget.audioPlayer.play(link);
    setState(() {
      widget.isMusicPlaying = true;
    });
  }

  Future<void> _pauseMusic() async {
    await widget.audioPlayer.pause();
    setState(() {
      widget.isMusicPlaying = false;
    });
  }
}
