import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:audioplayers/audioplayers.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MusicList(),
    );
  }
}

class MusicItem {
  final String musicFileLink;
  bool isPlaying;

  MusicItem({
    required this.musicFileLink,
    this.isPlaying = false,
  });
}

class MusicList extends StatefulWidget {
  @override
  _MusicListState createState() => _MusicListState();
}

class _MusicListState extends State<MusicList> {
  final FirebaseStorage _storage = FirebaseStorage.instance;
  List<String> musicFiles = []; // Liste des noms de fichiers musicaux
  List<String> musicLinks = []; // Liste des liens de fichiers musicaux

  AudioPlayer audioPlayer = AudioPlayer();
  String musicUrl = 'url_de_votre_musique.mp3';
  bool _music_started = false;
  Source audioUrl = UrlSource(
      'https://firebasestorage.googleapis.com/v0/b/wfapp-3d080.appspot.com/o/Maple%20Leaf%20Rag-mc.mp3?alt=media&token=4671a196-4070-4c3d-8b66-6a8de7e76809&_gl=1*1uxtzjw*_ga*Njg0OTU1MTU0LjE2OTQwOTU0Mjk.*_ga_CW55HF8NVT*MTY5ODk3NTg0Mi4zMi4xLjE2OTg5NzYyMTMuNjAuMC4w');

  @override
  void initState() {
    super.initState();
    // Récupérez la liste des fichiers musicaux depuis Firebase Storage
    // Remarque : vous devrez adapter cette partie en fonction de votre structure de stockage
    // par exemple, vous pouvez stocker les noms des fichiers dans Firebase Realtime Database.
    _fetchMusicList();
    _fetchDownloadLinks();
  }

  @override
  void dispose() {
    audioPlayer.dispose();
    super.dispose();
  }

  Future<void> _fetchMusicList() async {
    // Récupérez la liste des fichiers musicaux depuis Firebase Storage
    try {
      final result = await _storage.ref().listAll();
      setState(() {
        musicFiles = result.items.map((item) => item.name).toList();
      });
    } catch (e) {
      // Gérez les erreurs de récupération des données
      print('Erreur de récupération des données : $e');
    }
  }

  Future<List<String>> _fetchDownloadLinks() async {
    // Initialisez Firebase si ce n'est pas déjà fait
    // await Firebase.initializeApp();

    final FirebaseStorage storage = FirebaseStorage.instance;
    final ListResult result = await storage.ref().listAll();

    final List<String> downloadLinks = [];

    for (final Reference ref in result.items) {
      final String downloadURL = await ref.getDownloadURL();
      downloadLinks.add(downloadURL);
    }
    setState(() {
      musicLinks = downloadLinks;
    });
    return downloadLinks;
  }

  Future<void> playMusic(String musicLink) async {
    Source link = UrlSource(musicLink);
    await audioPlayer.play(link);
    setState(() {
      _music_started = true;
    });
  }

  Future<void> pauseMusic() async {
    await audioPlayer.pause();
    setState(() {
      _music_started = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste de Musique'),
      ),
      body: ListView.builder(
        itemCount: musicFiles.length,
        itemBuilder: (context, index) {
          final musicFile = musicFiles[index];
          String musicFileLink = musicLinks[index];
          return ListTile(
            title: Text(musicFile),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                !_music_started
                    ? IconButton(
                        icon: Icon(Icons.play_arrow),
                        onPressed: () {
                          playMusic(musicFileLink);
                        },
                      )
                    : IconButton(
                        icon: Icon(Icons.pause),
                        onPressed: () {
                          pauseMusic();
                        },
                      ),
                IconButton(
                  icon: Icon(Icons.cloud_download),
                  onPressed: () {
                    // Ajoutez ici la logique pour télécharger la musique depuis Firebase Storage
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
