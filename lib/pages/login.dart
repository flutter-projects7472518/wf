import 'package:BFolio/pages/musics.dart';
import 'package:BFolio/utilities.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  bool _showEmailError = false;
  bool _showPasswordError = false;

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('WF - Login'),
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/logo.png'),
            const SizedBox(height: 24.0),
            const Text(
              'CONNECTEZ-VOUS !',
              style: TextStyle(fontSize: 32.0),
            ),
            const SizedBox(height: 24.0),
            _buildEmailTextField(),
            const SizedBox(height: 24.0),
            _buildPasswordTextField(),
            const SizedBox(height: 24.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  child: isLoading
                      ? const SizedBox(
                          height: 30,
                          width: 30,
                          child: CircularProgressIndicator(
                              strokeWidth: 3, color: Colors.white),
                        )
                      : const Text('Se connecter'),
                  onPressed: () async {
                    setState(() {
                      // isLoading = true;
                      (emailController.text.isEmpty)
                          ? _showEmailError = true
                          : _showEmailError = false;

                      (passwordController.text.isEmpty)
                          ? _showPasswordError = true
                          : _showPasswordError = false;
                    });

                    if (emailController.text.isNotEmpty &&
                        passwordController.text.isNotEmpty) {
                      await loginToFirebase();

                      Future.delayed(const Duration(seconds: 3), () {
                        if (emailController.text.isNotEmpty ||
                            passwordController.text.isNotEmpty) {
                          setState(() {
                            isLoading = false;
                          });
                        } else {
                          setState(() {
                            isLoading = true;
                          });
                        }
                      });
                    }
                  },
                ),
              ],
            ),
            const SizedBox(height: 24.0),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SignupPage()));
              },
              child: const Text("Vous n'êtes pas encore membre ?"),
            ),
          ],
        ),
      ),
    );
  }

  TextField _buildEmailTextField() {
    return TextField(
      controller: emailController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.email),
        hintText: 'john@doe.test',
        labelText: 'Email *',
        errorText: _showEmailError ? 'Value Can\'t Be Empty' : null,
      ),
    );
  }

  bool _showPassword = false;

  Widget _buildPasswordTextField() {
    return TextField(
      controller: passwordController,
      obscureText: !_showPassword,
      decoration: InputDecoration(
        filled: true,
        labelText: 'Password',
        errorText: _showPasswordError ? 'Value Can\'t Be Empty' : null,
        icon: const Icon(Icons.security),
        suffixIcon: IconButton(
          icon: Icon(
            Icons.remove_red_eye,
            color: _showPassword
                ? Theme.of(context).colorScheme.primary
                : Colors.grey,
          ),
          onPressed: () {
            setState(() => _showPassword = !_showPassword);
          },
        ),
      ),
    );
  }

  Future loginToFirebase() async {
    try {
      isLoading = true;
      final credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);

      if (credential.additionalUserInfo?.isNewUser != null) {
        // ignore: use_build_context_synchronously
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const UploadPage()));
            // MaterialPageRoute(builder: (context) => const Musics()));
      }
    } on FirebaseAuthException catch (e) {
      isLoading = false;
      if (e.code == 'user-not-found') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('No user found for that email!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      } else if (e.code == 'invalid-email') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('Invalid Email provided for that user!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      } else if (e.code == 'wrong-password') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('Wrong password provided for that user!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      } else if (e.code == 'weak-password') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('Weak password provided for that user!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      } else if (e.code == 'invalid-password') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('Invalid password provided for that user!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      }
    }
  }
}
