import 'dart:io';

import 'package:BFolio/pages/generic.dart';
import 'package:BFolio/pages/musicslist.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';

class UploadPage extends StatefulWidget {
  const UploadPage({super.key});

  @override
  State<UploadPage> createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> {
  FirebaseStorage _storage = FirebaseStorage.instance;

  @override
  Widget build(BuildContext context) {
    return GenericPage(
      title: const Center(
        child: Text('WF - UPLOAD PAGE'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: _openFilePicker,
              child: const Text('Sélectionner un fichier'),
            ),
            const SizedBox(height: 20),
            const Text('Chemin du fichier sélectionné :'),
            Text(_filePath),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: _uploadFile,
              child: Text('Envoyer un fichier'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _uploadFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    if (result != null) {
      File file = File(result.files.single.path ?? '');
      Reference ref = _storage
          .ref()
          .child('dossier_destination/${result.files.single.name}');
      UploadTask task = ref.putFile(file);
      await task.whenComplete(() {
        print('Téléchargement terminé');
      });
    }
  }

  String _filePath = '';

  void _openFilePicker() async {
    String filePath = await FilePicker.platform.pickFiles().then((result) {
      if (result != null) {
        return Future.value(result.files.first.path);
      } else {
        return '';
      }
    });

    setState(() {
      _filePath = filePath;
    });
  }
}
