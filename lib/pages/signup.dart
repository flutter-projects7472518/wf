import 'package:BFolio/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({super.key});

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  String? dropdownvalue = 'Mr';

  var items = [
    'Mr',
    'Mme',
  ];

  final civilityController = TextEditingController();
  final lastNameController = TextEditingController();
  final firstNameController = TextEditingController();
  final addressController = TextEditingController();
  final addressComplementController = TextEditingController();
  final postalCodeController = TextEditingController();
  final cityController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final siretController = TextEditingController();

  var userId = '';
  bool isLoading = false;

  bool _showEmptyError = false;
  bool _showSamePasswordError = false;
  bool alreadySubmited = false;

  @override
  void dispose() {
    civilityController.dispose();
    lastNameController.dispose();
    firstNameController.dispose();
    addressController.dispose();
    addressComplementController.dispose();
    postalCodeController.dispose();
    cityController.dispose();
    phoneController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    siretController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('WF - Signup'),
      ),
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/logo.png'),
            const Text(
              'INSCRIVEZ-VOUS !',
              style: TextStyle(fontSize: 32.0),
            ),
            const SizedBox(height: 24.0),
            _buildCivilityDropdown(),
            const SizedBox(height: 24.0),
            _buildLastNameTextField(),
            const SizedBox(height: 24.0),
            _buildFirstNameTextField(),
            const SizedBox(height: 24.0),
            _buildAdressTextField(),
            const SizedBox(height: 24.0),
            _buildAdressComplementTextField(),
            const SizedBox(height: 24.0),
            _buildPostalCodeTextField(),
            const SizedBox(height: 24.0),
            _buildCityField(),
            const SizedBox(height: 24.0),
            _buildsiretField(),
            const SizedBox(height: 24.0),
            _buildPhoneTextField(),
            const SizedBox(height: 24.0),
            _buildEmailTextField(),
            const SizedBox(height: 24.0),
            _buildPasswordTextField(),
            const SizedBox(height: 24.0),
            _buildConfirmPasswordTextField(),
            const SizedBox(height: 24.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                    child: isLoading
                        ? const SizedBox(
                            height: 30,
                            width: 30,
                            child: CircularProgressIndicator(
                                strokeWidth: 3, color: Colors.white),
                          )
                        : const Text("S\'enregistrer"),
                    onPressed: () async {
                      setState(() {
                        alreadySubmited = true;
                      });
                      if (emailController.text.isEmpty ||
                          lastNameController.text.isEmpty ||
                          firstNameController.text.isEmpty ||
                          addressController.text.isEmpty ||
                          postalCodeController.text.isEmpty ||
                          cityController.text.isEmpty ||
                          phoneController.text.isEmpty ||
                          passwordController.text.isEmpty ||
                          confirmPasswordController.text.isEmpty ||
                          siretController.text.isEmpty) {
                        setState(() {
                          _showEmptyError = true;
                        });
                      } else {
                        setState(() {
                          _showEmptyError = false;
                        });
                        if (passwordController.text !=
                            confirmPasswordController.text) {
                          setState(() {
                            _showSamePasswordError = true;
                          });
                        } else {
                          setState(() {
                            _showSamePasswordError = false;
                          });

                          final uid = await signupUserForAuth();

                          Future.delayed(const Duration(seconds: 3), () {
                            if (emailController.text.isNotEmpty ||
                                passwordController.text.isNotEmpty) {
                              setState(() {
                                isLoading = false;
                              });
                            } else {
                              setState(() {
                                isLoading = true;
                              });
                            }
                          });

                          if (uid != null) {
                            setState(() {
                              userId = uid;
                            });
                            storeUserInfos();
                          }
                        }
                      }
                      ;
                    }),
              ],
            ),
            const SizedBox(height: 24.0),
          ],
        ),
      ),
    );
  }

  TextField _buildEmailTextField() {
    return TextField(
      controller: emailController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.email),
        hintText: 'john@doe.test',
        labelText: 'Email *',
        errorText: alreadySubmited && emailController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  bool _showPassword = false;
  Widget _buildPasswordTextField() {
    return TextField(
      controller: passwordController,
      obscureText: !_showPassword,
      decoration: InputDecoration(
        filled: true,
        labelText: 'Password',
        errorText: alreadySubmited && passwordController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : (_showSamePasswordError ? 'Mots de passe non identiques' : null),
        icon: const Icon(Icons.security),
        suffixIcon: IconButton(
          icon: Icon(
            Icons.remove_red_eye,
            color: _showPassword
                ? Theme.of(context).colorScheme.primary
                : Colors.grey,
          ),
          onPressed: () {
            setState(() => _showPassword = !_showPassword);
          },
        ),
      ),
    );
  }

  Widget _buildConfirmPasswordTextField() {
    return TextField(
      controller: confirmPasswordController,
      obscureText: !_showPassword,
      decoration: InputDecoration(
        filled: true,
        labelText: 'Confirm Password',
        errorText: alreadySubmited && confirmPasswordController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : (_showSamePasswordError ? 'Mots de passe non identiques' : null),
        icon: const Icon(Icons.security),
        suffixIcon: IconButton(
          icon: Icon(
            Icons.remove_red_eye,
            color: _showPassword
                ? Theme.of(context).colorScheme.primary
                : Colors.grey,
          ),
          onPressed: () {
            setState(() => _showPassword = !_showPassword);
          },
        ),
      ),
    );
  }

  Widget _buildCivilityDropdown() {
    return DropdownButton(
      value: dropdownvalue,
      icon: Icon(Icons.keyboard_arrow_down),
      items: items.map((items) {
        return DropdownMenuItem(value: items, child: Text(items));
      }).toList(),
      onChanged: (String? newValue) {
        setState(() {
          dropdownvalue = newValue;
        });
      },
    );
  }

  Widget _buildLastNameTextField() {
    return TextField(
      controller: lastNameController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.person_2),
        hintText: 'Doe',
        labelText: 'LastName *',
        errorText: alreadySubmited && lastNameController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  Widget _buildFirstNameTextField() {
    return TextField(
      controller: firstNameController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.person_2),
        hintText: 'John',
        labelText: 'FirstName *',
        errorText: alreadySubmited && firstNameController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  Widget _buildAdressTextField() {
    return TextField(
      controller: addressController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.home),
        hintText: '5 Place Roger Salengro',
        labelText: 'Address *',
        errorText: alreadySubmited && addressController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  Widget _buildAdressComplementTextField() {
    return TextField(
      controller: addressController,
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        filled: true,
        icon: Icon(Icons.home),
        hintText: 'Etage 8',
        labelText: 'Address Complement *',
      ),
    );
  }

  Widget _buildPostalCodeTextField() {
    return TextField(
      controller: postalCodeController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.messenger),
        hintText: '95140',
        labelText: 'Postal Code *',
        errorText: alreadySubmited && postalCodeController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  Widget _buildCityField() {
    return TextField(
      controller: cityController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.location_city),
        hintText: 'Garges-Les-Gonesse',
        labelText: 'City *',
        errorText: alreadySubmited && cityController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  Widget _buildsiretField() {
    return TextField(
      controller: siretController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.location_city),
        hintText: '111222333111',
        labelText: 'Siret *',
        errorText: alreadySubmited && siretController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  Widget _buildPhoneTextField() {
    return TextField(
      controller: phoneController,
      decoration: InputDecoration(
        border: const UnderlineInputBorder(),
        filled: true,
        icon: const Icon(Icons.phone),
        hintText: '0605678323',
        labelText: 'Phone *',
        errorText: alreadySubmited && phoneController.text.isEmpty
            ? 'Value Can\'t Be Empty'
            : null,
      ),
    );
  }

  signupUserForAuth() async {
    try {
      isLoading = true;
      final credential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      print("UID===================");
      print(credential.user?.uid);
      return credential.user?.uid;
    } on FirebaseAuthException catch (e) {
      isLoading = false;

      if (e.code == 'email-already-in-use') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('The account already exists for that email!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      } else if (e.code == 'weak-password') {
        setState(() {
          const snackBar = SnackBar(
            content: Text('The password provided is too weak!'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        });
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> storeUserInfos() async {
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    return users.doc(userId).set({
      'civility': dropdownvalue,
      'lastName': lastNameController.text,
      'firstName': firstNameController.text,
      'address': addressController.text,
      'complementAddress': addressComplementController.text,
      'postalCode': postalCodeController.text,
      'city': cityController.text,
      'phone': phoneController.text,
      'email': emailController.text,
      'siret': siretController.text,
    }).then((value) {
      setState(() {
        const snackBar = SnackBar(
          content: Text('Inscription terminée avec succès!'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
    }).catchError((error) {
      print("Failed to add user: $error");
      setState(() {
        const snackBar = SnackBar(
          content: Text("Erreur lors de l'inscription!"),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    });
  }
}
