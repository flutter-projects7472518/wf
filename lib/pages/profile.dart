import 'package:BFolio/pages/generic.dart';
import 'package:flutter/material.dart';
import 'package:profile/profile.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return GenericPage(
        title: const Text("Mon Profil"),
        body: Center(
          child: Profile(
            imageUrl: "https://b-folio.000webhostapp.com/img/beh.jpg",
            name: "C. Beh Ardieu ALOTIN",
            website: "https://bfoliopro-4a9aa851dac0.herokuapp.com/",
            designation: "Consultant dev Web et Mobile",
            email: "alotinbehardieu@gmail.com",
            phone_number: "0605678323",
          ),
        ));
  }
}
