import 'dart:io';

import 'package:BFolio/pages/generic.dart';
import 'package:BFolio/pages/musicslist.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Musics extends StatefulWidget {
  const Musics({super.key});

  @override
  State<Musics> createState() => _MusicsState();
}

class _MusicsState extends State<Musics> {
  File _audioFile = File('');
  String _downloadUrl = '';

  final FirebaseStorage _storage = FirebaseStorage.instance;

  Future<void> _uploadFile() async {
    final storageReference = _storage.ref().child(basename(_audioFile.path));
    await storageReference.putFile(_audioFile);
    setState(() {
      _downloadUrl = '';
    });
  }

  Future<void> _downloadFile() async {
    final storageReference = _storage.ref().child(basename(_audioFile.path));
    final url = await storageReference.getDownloadURL();
    setState(() {
      _downloadUrl = url;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Upload/Download Audio'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _audioFile == null
                ? const Text('Sélectionnez un fichier audio...')
                : Text('Fichier sélectionné: ${basename(_audioFile.path)}'),
            ElevatedButton(
              onPressed: () => _uploadFile(),
              child: const Text('Uploader'),
            ),
            ElevatedButton(
              onPressed: () => _downloadFile(),
              child: const Text('Télécharger'),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    // MaterialPageRoute(builder: (context) => const UploadPage()));
                    MaterialPageRoute(builder: (context) => MusicList()));
              },
              child: Text('Liste des musiques'),
            ),
            if (_downloadUrl != null)
              Column(
                children: [
                  const Text('URL de téléchargement:'),
                  Text(_downloadUrl),
                ],
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final audioFile = await FilePicker.platform.pickFiles(
            type: FileType.custom,
            allowedExtensions: ['mp3', 'wav'],
          );
          if (audioFile != null) {
            setState(() {
              _audioFile = File(audioFile.files.single.path ?? "");
              _downloadUrl = '';
            });
          }
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
